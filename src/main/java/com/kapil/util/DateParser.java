/* Created by kapil on 1/18/2017. */

package com.kapil.util;

import com.kapil.exception.EErrorCode;
import com.kapil.exception.FlightFinderException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DateParser {

    private static final List<String> validDateFormats = new ArrayList<>(Arrays.asList("MM/dd/yyyy HH:mm:ss", "MM-dd-yyyy HH:mm:ss"));

    private DateParser() {

    }

    /*
    * This function basically only validates our dates by making sure they are parsed without an exception 
    */
    public static Date parseDate(String date) throws FlightFinderException {
        Date parsedDate;
        for (String dateFormat : validDateFormats) {
            parsedDate = parseDateWithFormat(dateFormat, date);
            if (parsedDate != null) {
                return parsedDate;
            }
        }
        throw new FlightFinderException(EErrorCode.INTERNAL_SERVICE_ERROR, "Invalid date format in the manifest");
    }

    private static Date parseDateWithFormat(String dateFormat, String dateString) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(dateFormat);
            return df.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String parseInOutputDateFormat(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/YYYY HH:mm:ss");
        return simpleDateFormat.format(date);
    }
}
