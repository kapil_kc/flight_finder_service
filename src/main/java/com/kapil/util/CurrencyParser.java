/* Created by kapil on 1/18/2017. */

package com.kapil.util;

import java.math.BigDecimal;

public class CurrencyParser {

    private CurrencyParser() {
    }

    public static BigDecimal parseCurrency(String price) {
        price = price.replace("$", "");
        return new BigDecimal(price);
    }
}
