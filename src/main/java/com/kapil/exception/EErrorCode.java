/* Created by kapilc on 18/01/2017. */

package com.kapil.exception;

public enum EErrorCode {
    OK(0),
    GENERIC_ERROR(1),
    USER_ERROR(2),
    USER_INPUT_ERROR(3),
    INTERNAL_SERVICE_ERROR(4);

    private final int code;

    EErrorCode(int code) {
        this.code = code;
    }
}
