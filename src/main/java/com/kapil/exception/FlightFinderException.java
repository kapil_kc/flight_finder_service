/* Created by kapil on 1/19/2017. */

package com.kapil.exception;

public class FlightFinderException extends Exception {

    private static final long serialVersionUID = -5481263928015647358L;

    private EErrorCode errorCode;
    private Object[] args;

    public FlightFinderException() {
        super();
    }

    public FlightFinderException(String message) {
        super(message);
    }

    public FlightFinderException(Throwable cause) {
        super(cause);
    }

    public FlightFinderException(String message, Throwable cause) {
        super(message, cause);
    }

    public FlightFinderException(EErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public FlightFinderException(EErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public FlightFinderException(EErrorCode errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public FlightFinderException(EErrorCode errorCode, Throwable cause, Object[] args) {
        super(cause);
        this.errorCode = errorCode;
        this.args = args;
    }

    public EErrorCode getErrorCode() {
        return errorCode;
    }
}
