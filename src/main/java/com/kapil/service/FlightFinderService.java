/* Created by kapilc on 18/01/2017. */

package com.kapil.service;

import com.kapil.model.Flight;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class FlightFinderService {

    private String origin;
    private String destination;
    private Set<String> fileSet;
    private Set<Flight> resultSet = new HashSet<>();

    public FlightFinderService(String origin, String destination) {

        fileSet = new HashSet<>();
        fileSet.add("Provider1.txt");
        fileSet.add("Provider2.txt");
        fileSet.add("Provider3.txt");

        this.origin = origin;
        this.destination = destination;
    }

    public FlightFinderService(Set<String> fileSet, String origin, String destination) {
        this.fileSet = fileSet;
        this.origin = origin;
        this.destination = destination;
    }

    public Set<Flight> getFlights() throws IOException {

        for (String fileName : fileSet) {
            getFlightsFromCurrentSet(fileName);
        }

        resultSet.remove(null);
        return resultSet;
    }

    private void getFlightsFromCurrentSet(String fileName) throws IOException {
        InputStream inputStream = loadFile(fileName);
        int count = 2;
        FilterService filterService;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"))) {
            String line;
            filterService = new FilterService(br.readLine());
            if (filterService.isValid()) {
                while ((line = br.readLine()) != null) {
                    resultSet.add(filterService.validateAndFilterFlight(line, origin, destination, fileName, count));
                    count++;
                }
            }
        }
    }

    private InputStream loadFile(String fileName) throws IOException {
        InputStream inputStream = this.getClass().getResourceAsStream("/" + fileName);
        if (inputStream == null) {
            throw new FileNotFoundException("Requested resource cannot be found!!");
        }
        return inputStream;
    }

    public static Set<Flight> sortFlightsBasedOnPriceAndDateOfDeparture(Set<Flight> flights) {
        Set<Flight> sortedFlightInfo = new TreeSet<>((flight1, flight2) -> {
            if (flight1.getTicketPrice().equals(flight2.getTicketPrice())) {
                return flight1.getDepartureDate().compareTo(flight2.getDepartureDate());
            }
            return flight1.getTicketPrice().compareTo(flight2.getTicketPrice());
        });

        sortedFlightInfo.addAll(flights);
        return sortedFlightInfo;
    }

}
