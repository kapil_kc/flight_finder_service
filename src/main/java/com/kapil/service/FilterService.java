/* Created by kapil on 1/19/2017. */

package com.kapil.service;

import com.kapil.model.Flight;
import com.kapil.util.CurrencyParser;
import com.kapil.util.DateParser;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FilterService {

    private String currentSeparator;
    private boolean valid;

    private static final List<String> validSeparators = new ArrayList<>(Arrays.asList("|", ","));

    public FilterService(String flightInfoManifest) {
        for (String separator : validSeparators) {
            if (flightInfoManifest.split(separator).length == 5) {
                this.currentSeparator = separator;
                this.valid = true;
                break;
            }
        }
    }

    public Flight validateAndFilterFlight(String flightInfo, String origin, String destination, String fileName, int lineNumber) {

        String flightOrigin;
        String flightDestination;
        String departureTime;
        String arrivalTime;
        String price;

        Flight flight;

        if (StringUtils.isEmpty(currentSeparator) || flightInfo.split(currentSeparator).length != 5) {
            return null;
//            throw new FlightFinderException(EErrorCode.INTERNAL_SERVICE_ERROR, fileName + " contains invalid flight details on line number " + lineNumber);
        }

        String[] flightInfoArray = flightInfo.split(currentSeparator);

        flightOrigin = flightInfoArray[0];
        departureTime = flightInfoArray[1];
        flightDestination = flightInfoArray[2];
        arrivalTime = flightInfoArray[3];
        price = flightInfoArray[4];

        if (origin.equals(flightOrigin) && destination.equals(flightDestination)) {
            try {
                flight = new Flight();
                flight.setOrigin(origin);
                flight.setDestination(destination);
                flight.setDepartureDate(DateParser.parseDate(departureTime));
                flight.setArrivalDate(DateParser.parseDate(arrivalTime));
                flight.setTicketPrice(CurrencyParser.parseCurrency(price));
                flight.setInfoString(composeInfoString(flight, price));
                return flight;
            } catch (Exception e) {
                return null;
            }
        }

        return null;
    }

    private String composeInfoString(Flight flight, String price) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(flight.getOrigin()).append(" --> ");
        stringBuilder.append(flight.getDestination()).append(" (");
        stringBuilder.append(DateParser.parseInOutputDateFormat(flight.getDepartureDate())).append(" --> ");
        stringBuilder.append(DateParser.parseInOutputDateFormat(flight.getArrivalDate())).append(") - ");
        stringBuilder.append(price);
        return stringBuilder.toString();
    }

    public String getCurrentSeparator() {
        return currentSeparator;
    }

    public void setCurrentSeparator(String currentSeparator) {
        this.currentSeparator = currentSeparator;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
