/* Created by kapilc on 18/01/2017. */

package com.kapil.model;

import com.beust.jcommander.Parameter;

public class FlightFinderModel {

    @Parameter(names = {"-o"}, description = "Origin", required = true)
    private String origin;

    @Parameter(names = {"-d"}, description = "Destination", required = true)
    private String destination;

    @Parameter(names = "-help", help = true, description = "Prints options and their usage!")
    private boolean help;

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public boolean isHelp() {
        return help;
    }

}
