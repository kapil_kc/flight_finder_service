/* Created by kapil on 1/18/2017. */

package com.kapil.model;

import java.math.BigDecimal;
import java.util.Date;

public class Flight {

    private String origin;
    private String destination;
    private Date departureDate;
    private Date arrivalDate;
    private BigDecimal ticketPrice;
    private String infoString;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getInfoString() {
        return infoString;
    }

    public void setInfoString(String infoString) {
        this.infoString = infoString;
    }

    @Override
    public String toString() {
        return infoString;
    }

    @Override
    public int hashCode() {
        return infoString.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Flight && infoString.equals(((Flight) obj).getInfoString());
    }
}
