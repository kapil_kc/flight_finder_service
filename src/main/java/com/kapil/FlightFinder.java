/* Created by kapilc on 18/01/2017. */

package com.kapil;

import com.beust.jcommander.JCommander;
import com.kapil.model.Flight;
import com.kapil.model.FlightFinderModel;
import com.kapil.service.FlightFinderService;

import java.io.IOException;
import java.util.Set;

public class FlightFinder {

    private boolean loading = true;


    public static void main(String[] args) {

        try {
            FlightFinderModel finderModel = new FlightFinderModel();
            JCommander commander = new JCommander();
            commander.addObject(finderModel);
            commander.parse(args);

            if (finderModel.isHelp()) {
                commander.usage();
                return;
            }

            FlightFinderService flightFinderService = new FlightFinderService(finderModel.getOrigin(), finderModel.getDestination());

            Set<Flight> flights = FlightFinderService.sortFlightsBasedOnPriceAndDateOfDeparture(flightFinderService.getFlights());

            if (flights.isEmpty()) {
                System.out.println("No flights found for " + finderModel.getOrigin() + " --> " + finderModel.getDestination());
            }

            for (Flight flight : flights) {
                System.out.println(flight.getInfoString());
            }

        } catch (Exception ex) {
            System.out.println("\n" + ex.getMessage());
            System.exit(1);
        }

    }


    private synchronized void loading(String message) throws IOException, InterruptedException {
        System.out.println("\n" + message);
        Thread th = new Thread() {
            @Override
            public void run() {
                try {
                    System.out.write("\r|".getBytes());
                    while (loading) {
                        System.out.write("->".getBytes());
                        Thread.sleep(500);
                    }

                    System.out.write("| Done \r\n\n".getBytes());


                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        th.start();
    }

}
