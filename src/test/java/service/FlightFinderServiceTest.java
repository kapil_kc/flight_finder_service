/* Created by kapilc on 19/01/2017. */

package service;

import com.kapil.model.Flight;
import com.kapil.service.FlightFinderService;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FlightFinderServiceTest {

    @Test
    public void testFlightInfoPrimaryAndSecondarySort() {
        Flight flight = new Flight();
        flight.setTicketPrice(new BigDecimal("150.00"));
        flight.setDepartureDate(new Date());
        flight.setInfoString("Should be 1");
        Flight flight2 = new Flight();
        flight2.setTicketPrice(new BigDecimal("180.00"));
        flight2.setDepartureDate(new Date());
        flight2.setInfoString("Should be 2");
        Flight flight3 = new Flight();
        flight3.setTicketPrice(new BigDecimal("200.00"));
        flight3.setDepartureDate(getFutureDate(10));
        flight3.setInfoString("Should be 3");
        Flight flight4 = new Flight();
        flight4.setTicketPrice(new BigDecimal("200.00"));
        flight4.setDepartureDate(getFutureDate(12));
        flight4.setInfoString("Should be 4");

        Set<Flight> flightSet = new HashSet<>();
        flightSet.add(flight4);
        flightSet.add(flight3);
        flightSet.add(flight2);
        flightSet.add(flight);

        flightSet = FlightFinderService.sortFlightsBasedOnPriceAndDateOfDeparture(flightSet);
        int count = 1;
        for (Flight f : flightSet) {
            assertEquals("Should be " + count, f.getInfoString());
            count++;
        }

    }

    @Test
    public void testFlightFinderService() throws IOException {
        String textFile = "test/TestProvider.txt";
        Set<String> fileSet = new HashSet<>();
        fileSet.add(textFile);
        FlightFinderService flightFinderService = new FlightFinderService(fileSet, "YVR", "YYZ");
        Set<Flight> flights = flightFinderService.getFlights();
        assertTrue(flights.size() == 1);
        for (Flight f : flights) {
            assertEquals("YVR --> YYZ (06/18/2014 09:10:00 --> 06/18/2014 19:47:00) - $1093.00", f.getInfoString());
        }
    }

    private Date getFutureDate(int numberOfDaysInFuture) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, numberOfDaysInFuture);
        return cal.getTime();
    }
}
