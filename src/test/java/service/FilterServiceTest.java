/* Created by kapilc on 19/01/2017. */

package service;

import com.kapil.model.Flight;
import com.kapil.service.FilterService;
import org.junit.Test;

import static org.junit.Assert.*;

public class FilterServiceTest {

    private static final String validHeaderString = "Origin, destination, a, b, c";
    private static final String validSampleFlightInfo = "LAS,01/01/2014 13:30:00,LAX,01/01/2014 14:30:00,$150.00";

    @Test
    public void checkInvalidSeparatorIsCaught() {
        String invalidString = "Origin, destination, a, b, c, d, e, f";
        FilterService filterService = new FilterService(invalidString);
        assertFalse(filterService.isValid());
    }

    @Test
    public void checkValidSeparatorIsWorking() {
        FilterService filterService = new FilterService(validHeaderString);
        assertTrue(filterService.isValid());
    }

    @Test
    public void testValidationAndFlightFiltering() {
        Flight flight;

        FilterService filterService = new FilterService(validHeaderString);
        assertTrue(filterService.isValid());

        try {
            flight = filterService.validateAndFilterFlight(validSampleFlightInfo, "LAS", "LAX", null, 0);
            assertTrue(flight != null);
            assertEquals("LAS --> LAX (01/01/2014 13:30:00 --> 01/01/2014 14:30:00) - $150.00", flight.getInfoString());

        } catch (Exception e) {
            fail("Exception should not have been thrown");
        }

    }

    @Test
    public void testValidationWithOriginDestinationMisMatch() {
        Flight flight;

        FilterService filterService = new FilterService(validHeaderString);
        assertTrue(filterService.isValid());

        try {
            flight = filterService.validateAndFilterFlight(validSampleFlightInfo, "LAS", "YVR", null, 0);
            assertTrue(flight == null);
        } catch (Exception e) {
            fail("Exception should not have been thrown");
        }
    }

    @Test
    public void testValidationAndFilteringWithFaultyManifest() {
        String invalidSampleFlightInfo = "LAS,01a/01/2014 13:30:00,LAX,01/01/2014 14:30:00,$150.00";
        Flight flight;

        FilterService filterService = new FilterService(validHeaderString);
        assertTrue(filterService.isValid());

        try {
            flight = filterService.validateAndFilterFlight(invalidSampleFlightInfo, "LAS", "LAX", null, 0);
            assertTrue(flight == null);
        } catch (Exception e) {
            fail("Exception should not have been thrown");
        }
    }

}
