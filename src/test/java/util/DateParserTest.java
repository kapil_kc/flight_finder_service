/* Created by kapilc on 19/01/2017. */

package util;

import com.kapil.exception.EErrorCode;
import com.kapil.exception.FlightFinderException;
import com.kapil.util.DateParser;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class DateParserTest {

    @Test
    public void testOutputFormatDateParsing() {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2016-01-01 00:00:00");
            String outputDateFormat = DateParser.parseInOutputDateFormat(date);
            assert (outputDateFormat.equals("01/01/2016 00:00:00"));
        } catch (Exception e) {
            fail("Exception should not have been thrown");
        }
    }

    @Test
    public void testDateParsingException() {
        try {
            String invalidDate = "Invalid_Date";
            DateParser.parseDate(invalidDate);
            fail("Exception should have been thrown");
        } catch (FlightFinderException e) {
            assertTrue(e.getErrorCode().equals(EErrorCode.INTERNAL_SERVICE_ERROR));
            assertTrue(e.getMessage().equals("Invalid date format in the manifest"));
        }
    }

    @Test
    public void testDateParsing() {
        try {
            String validDate = "01/01/2016 12:30:00";
            Date date = DateParser.parseDate(validDate);
            assertTrue(date.toString().equals("Fri Jan 01 12:30:00 EST 2016"));
        } catch (FlightFinderException e) {
            fail("Exception should not have been thrown");
        }
    }

}
