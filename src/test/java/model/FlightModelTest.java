/* Created by kapilc on 19/01/2017. */

package model;

import com.kapil.model.Flight;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class FlightModelTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    /*
     * Since we use Set<Flight> in our service,
     * we need to make sure that two flights that are same logically do not end up duplicating in the set
    */
    @Test
    public void testFlightModelEquality() {
        Set<Flight> flightSet = new HashSet<>();
        Flight f1 = new Flight();
        f1.setInfoString("flight-1");
        Flight f2 = new Flight();
        f2.setInfoString("flight-2");
        Flight f3 = new Flight();
        f3.setInfoString("flight-1");
        flightSet.add(f1);
        flightSet.add(f2);
        flightSet.add(f3);
        assertTrue(flightSet.size() == 2);
        List<String> infoStringList = flightSet.stream().map(Flight::getInfoString).collect(Collectors.toList());
        assertTrue(infoStringList.contains("flight-1"));
        assertTrue(infoStringList.contains("flight-2"));

    }

}
