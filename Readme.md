Clone the project on your local machine

Navigate into the project's directory

Build the project with mvn clean install

There will be a searchFlights-jar-with-dependencies.jar in project's target folder. 

Copy the jar to a desired folder and navigate to that folder and run the utility with following command: 

java -jar searchFlights-jar-with-dependencies.jar -o {origin} -d {destination}